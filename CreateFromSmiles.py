from BALL import MOL2File, System, SDGenerator, SmilesParser, File
import sys

sp = SmilesParser()
sp.parse(sys.argv[1])

s = sp.getSystem()

sdg = SDGenerator(True)
sdg.generateSD(s)

outfile = MOL2File(sys.argv[2], File.MODE_OUT)
outfile.write(s)
outfile.close()
