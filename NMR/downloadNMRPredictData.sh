#!/bin/bash

# remember the directory from which we were called
olddir=$(pwd)

# change into our pipeline base directory
cd $(dirname $0)

source $(dirname $0)/NMR_pipeline_definitions.sh

export PATH=$(readlink -f $(dirname $0)/build):$PATH

###################################################

# TODO do we need this or is it handled by galaxy?
if ${single_user}; then	
	logdir=${logdir}$$/
	resultsdir=${resultsdir}$$/
fi

mkdir $datadir      2> /dev/null # TODO one universal data repository?
mkdir $resultsdir   2> /dev/null
mkdir $logdir       2> /dev/null

# explain how to use the script
NO_ARGS=0 
E_OPTERROR=85
if [ $# -eq "$NO_ARGS" ]    # Script invoked with no command-line args?
then
  echo "Usage: `basename $0` options (--predtype=[PureProtein|ProteinLigand] --outfile=... )" 
  exit $E_OPTERROR          # Exit and explain usage.
                            # Usage: scriptname -options
                            # Note: dash (-) necessary
fi

# parse the command line arguments
while getopts ":-:" Option; do
  case $Option in
		- )
			case "${OPTARG}" in
				outfile=*)
					export outfile=${OPTARG#*=}	
          ;;
        predtype=*)
					export prediction_type=${OPTARG#*=}
				  ;;
			esac
  esac
done

# check command line arguments
if [ "${prediction_type}" = "" ]; then
  echo "Error: invalid prediction type!"
  exit $E_OPTERROR  
fi

if [ "${prediction_type}" = "PureProtein" ]; then
	echo "  Pure Protein case"
elif [ "${prediction_type}" = "ProteinLigand" ]; then
	#TODO Protein Ligand case
	echo "  The Protein - Ligand option is currently not yet implemented. Sorry!"
	exit
else	
	echo "  Unknown prediction type ", ${prediction_type}
	exit $E_OPTERROR
fi

if [ "${outfile}" = "" ]; then
  echo "Error: invalid outfile!"
  exit $E_OPTERROR  
fi


echo "**************************************************************************"
echo "  Download data... (this may take a while...)"
echo "     (see ${logdir}download_pdb_and_bmrb.log)"
echo "     (find mappings in ${mappingfilename} etc.)"
echo "**************************************************************************"
echo " Call getPDBtoBMRB.py --> mapping files ..."
python getPDBandBMRBFiles.py --mappingfilename=${mappingfilename} \
                             --mappingfilename_ligand=${mappingfilename_ligand} \
														 --mappingfilename_dna=${mappingfilename_dna} \
														 --mappingfilename_dna_ligand=${mappingfilename_dna_ligand} \
													   > ${logdir}download_pdb_and_bmrb.log 2>&1


# create a unique mapping file for the ballaxy user
if [ "${prediction_type}" = "PureProtein" ]; then
	cp ${mappingfilename} ${outfile}
elif [ "${prediction_type}" = "ProteinLigand" ]; then
	cp ${mappingfilename_ligand} ${outfile}
fi	

# just for testing purposes
#cp /home/HPL/anne/Desktop/mapping_50_test.txt  ${outfile}

# go back
cd ${olddir}

echo " Done."
