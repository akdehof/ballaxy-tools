#!/usr/bin/python

import sys, os
import sqlite3
import urllib2, urllib, xml.dom.minidom
import re
import BALL
import pprint
import time
import tempfile
import shutil

from nmr_definitions import *



def runPISCES(mappingfilename, pisces_maxpc):
  # we rely on environment variables set by the pipeline
  if not (os.path.exists(pisces_binary) and os.access(pisces_binary, os.X_OK)):
    print("Could not execute PISCES binary! Exiting!")
    exit(1)
  # PISCES should be called from its installation directory
  olddir = os.getcwd()
  os.chdir(pisces_path)
  # finding the results of PISCES is not trivial... the naming scheme is a little
  # strange. so we take a timestamp now and move all newer files later on
  before_pisces_stamp=time.time()

  # create the input file with the list of all chains
  (pisces_file, pisces_input) = tempfile.mkstemp()

  for line in open(mappingfilename, 'r').readlines():
    os.write(pisces_file, line.split()[0]+"\n")
  os.close(pisces_file)

  # call pisces
  os.system("%s -p %s %s -i %s" % (pisces_binary, pisces_maxpc, pisces_parameters, pisces_input))

  # now, find all files that were created by pisces
  resultfile = "%s/culled_chains_%spercent.txt" % (datadir, pisces_maxpc)
  for lfile in os.listdir("."):
    if os.path.isfile(lfile):
      if (os.stat(lfile).st_ctime > before_pisces_stamp) or (os.stat(lfile).st_mtime > before_pisces_stamp):
        # this should match the real result
        if (re.match("(.*)_chains[0-9]+$", lfile)): 
          shutil.move(lfile, resultfile)
        else:
          # clean up
          os.remove(lfile)

  os.chdir(olddir)
  os.remove(pisces_input)

  return resultfile


####################################################
##
##
##              MAIN
##
##
####################################################
if (len(sys.argv) < 2):
  print("Usage: %s  --mappingfile=... (--pisces_maxpc=... | --homology_file=...) [--DB=... --colname=...] [--outfile=...]" % sys.argv[0])
  exit(1)

colname = ""
homology_filename = ""
nmr_sqlite_db=""

# parse the command line arguments
command_args = {}
for arg in sys.argv[1:]:
  sp = arg.split("=")
  command_args[sp[0][2:]] = sp[1]

#import pprint
#pprint.pprint(command_args)

### mapping file
mappingfilename=""
if not command_args.has_key("mappingfile"):
  print("Error: No mapping file given!")
  exit (1)
mappingfilename = command_args["mappingfile"]


### DB
if not command_args.has_key("DB"):
  print("Warning: No database given!")
else:
 nmr_sqlite_db = command_args["DB"]

### colname
if (not command_args.has_key("colname")):
  print("Warning: No homology restriction column name given!")
else:
  colname = command_args["colname"]

if ( not (command_args.has_key("DB") == (command_args.has_key("colname")))):
  print("Error: Inconsistency for parameters DB and colname - both are needed!")
  exit (1)


### pisces_maxpc
pisces_maxpc=10
if (not command_args.has_key("pisces_maxpc")):
  print("Warning: No homology restriction percentage given!")
else:
  pisces_maxpc = command_args["pisces_maxpc"]

if (command_args.has_key("pisces_maxpc") == command_args.has_key("homology_file")):
  print("Error: Inconsistent parameters pisces_maxpc and homology_file!")
  exit (1)

### homology file
homology_filename=""
if command_args.has_key("homology_file"):
  homology_filename = command_args["homology_file"]
else:
  print "Run the pisces service..."
  # run PISCES to compute homology
  homology_filename = runPISCES(mappingfilename, pisces_maxpc)
  # TODO: > homology_filename
  print "Homology restriction stored in file: ", homology_filename


### outfile
outfile=""
if command_args.has_key("outfile"):
  outfile = command_args["outfile"]



##################
#
#       MAIN
#
##################
# read the non homolog set
non_homolog = open(homology_filename, 'r').readlines()

# add the non homolog information to the DB
non_homologs =  {}
# connect to the db
if (nmr_sqlite_db != ""):
  print "Connecting to DB " + nmr_sqlite_db
  conn = sqlite3.connect(nmr_sqlite_db)

  # try to add a column to the database
  try:
    conn.execute("alter table PDB_BMRB add column %s INTEGER default 0" % colname)
  except:
    print("Column %s already present" % colname)

  # the first line contains a header
  for line in non_homolog[1:]:
    entry = line.split()[0]
    pdb_id = entry[0:4].upper()
    chain = entry[4].upper()
    non_homologs[pdb_id] = chain
    #   add information to the database
    conn.execute("update PDB_BMRB set %s=1 where PDB_ID=\'%s\' and PDB_CHAIN_ID=\'%s\'" % (colname, pdb_id, chain))

  conn.commit()
  conn.close()
else: # else simply store the not homolog set
  for line in non_homolog[1:]:  
    entry = line.split()[0]
    pdb_id = entry[0:4].upper()
    chain = entry[4].upper()
    non_homologs[pdb_id] = chain

# write the new mapping
old_mappings = open(mappingfilename, 'r').readlines()
if (outfile == ""):
  outfile = mappingfilename
f = open(outfile, 'w')
for line in old_mappings:
  pdb_id = line.split()[0]
  #print  pdb_id
  if (non_homologs.has_key(pdb_id)):
    #print line
    f.write(line[:-1] + " " + non_homologs[pdb_id] + "\n")
f.close()

print "Done."
# done.
