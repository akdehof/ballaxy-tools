from os import environ

datadir    = environ["datadir"]
resultsdir = environ["resultsdir"]
logdir     = environ["logdir"]

mappingfilename            = environ["mappingfilename"]
mappingfilename_ligand     = environ["mappingfilename_ligand"]
mappingfilename_dna        = environ["mappingfilename_dna"]
mappingfilename_dna_ligand = environ["mappingfilename_dna_ligand"]

nmr_sqlite_db = environ["nmr_sqlite_db"]

pdbhome             = environ["pdbhome"]
pdbhome_ligand      = environ["pdbhome_ligand"]
pdbhome_dna         = environ["pdbhome_dna"]
pdbhome_dna_ligand  = environ["pdbhome_dna_ligand"]
pdbhome_shiftx2_set = environ["pdbhome_shiftx2_set"]

bmrbhome            = environ["bmrbhome"]
bmrbhome_ligand     = environ["bmrbhome_ligand"]
bmrbhome_dna        = environ["bmrbhome_dna"]
bmrbhome_dna_ligand = environ["bmrbhome_dna_ligand"]
bmrbhome_refdb      = environ["bmrbhome_refdb"]

source_dir = environ["source_dir"]

pisces_path       = environ["pisces_path"]
pisces_binary     = environ["pisces_binary"] 
pisces_maxpc      = environ["pisces_maxpc"]
pisces_parameters = environ["pisces_parameters"]

shiftx2_path = environ["shiftx2_path"]
