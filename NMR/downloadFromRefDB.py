import urllib2, sys, os

from nmr_definitions import *

basepath = "http://mackenzie.wishartlab.com/RefDB/corrdata/bmr%s.str.corr"
outpath = bmrbhome_refdb+"bmr%s_corr.str"

mapping = open(mappingfilename, 'r').readlines()

for m in mapping:
  print m
  bmr = m.split()[1]
  outfile = open(outpath  % bmr, 'w')
  try:
    f = urllib2.urlopen(basepath % bmr)
    outfile.write(f.read())
    outfile.close()
  except:
    print "Not found"
    outfile.close()
    os.unlink(outfile.name)
