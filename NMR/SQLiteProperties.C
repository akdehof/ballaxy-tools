// -*- Mode: C++; tab-width: 2; -*-
// vi: set ts=2:
//

/*
		NightShift: NMR shift inference by General Hybrid model Training 
		 --- A pipeline for automated NMR shift prediction training ---
		
		Anna Dehof
*/

// ----------------------------------------------------
// $Maintainer: Anna Dehof $
// $Authors:    Anna Dehof $
// ----------------------------------------------------


#include <BALL/FORMAT/commandlineParser.h>
#include <BALL/FORMAT/NMRStarFile.h>
#include <BALL/FORMAT/PDBFile.h>
#include <BALL/FORMAT/lineBasedFile.h>
#include <BALL/SYSTEM/path.h>
#include <BALL/NMR/shiftModel.h>
#include <BALL/NMR/empiricalHSShiftProcessor.h>
#include <BALL/NMR/haighMallionShiftProcessor.h>
#include <BALL/NMR/randomCoilShiftProcessor.h>
#include <BALL/NMR/EFShiftProcessor.h>
#include <BALL/NMR/HBondShiftProcessor.h>

#include <BALL/STRUCTURE/secondaryStructureProcessor.h>
#include <BALL/STRUCTURE/HBondProcessor.h>
#include <BALL/STRUCTURE/geometricProperties.h>
#include <BALL/STRUCTURE/peptides.h>
#include <BALL/STRUCTURE/residueChecker.h>
#include <BALL/STRUCTURE/defaultProcessors.h>
#include <BALL/STRUCTURE/assignBondOrderProcessor.h>
#include <BALL/STRUCTURE/connectedComponentsProcessor.h>
#include <BALL/STRUCTURE/rotamerLibrary.h>
#include <BALL/STRUCTURE/fragmentDB.h>
#include <BALL/DATATYPE/hashGrid.h>
#include <BALL/MOLMEC/AMBER/amber.h>
#include <BALL/MOLMEC/MINIMIZATION/conjugateGradient.h>
#include <BALL/MOLMEC/AMBER/GAFFTypeProcessor.h>
#include <BALL/KERNEL/PTE.h>
#include <BALL/KERNEL/selector.h>

#include <fstream>
#include <iostream>
#include <math.h>
#include <propertiesForShift.h>
#include <SQLiteConnector.h>

#define ROTAMER_MAX_IDX 30

using namespace BALL;
using namespace std;

// not nice, but simple...
vector<String> current_query_result;

int ballback(void *NotUsed, int argc, char** argv, char **azColName)
{
	current_query_result.clear();
	String result = "";
	for (int i=0; i<argc; ++i)
	{
		result += String(argv[i]) + "|";
	}

	current_query_result.push_back(result);
	return 0;
}

bool isAminoAcid(Atom* a)
{
	if (!a) return false;

	if (a->getResidue())
	{
		String name = a->getResidue()->getName();
		if (   name=="ALA" || name=="ARG" || name=="ASN"
		    || name=="ASP" || name=="CYS" || name=="GLU"
				|| name=="GLN" || name=="GLY" || name=="HIS"
				|| name=="ILE" || name=="LEU" || name=="LYS"
				|| name=="MET" || name=="PHE" || name=="PRO"
				|| name=="SER" || name=="THR" || name=="TRP"
				|| name=="TYR" || name=="VAL")
		{
			return true;
		}
	}

	return false;
}

bool hasAminoAcid(std::vector<Atom*>& component)
{
	for (Position i=0; i<component.size(); ++i)
	{
		Atom* a = component[i];

		if (isAminoAcid(a))
		{
			return true;
		}
	}
	return false;
}

bool isIon(Atom* a)
{
	if (   (a->getElement() == PTE[Element::Ca])
			|| (a->getElement() == PTE[Element::Cu])
			|| (a->getElement() == PTE[Element::Zn])
			|| (a->getElement() == PTE[Element::Mg])
			|| (a->getElement() == PTE[Element::Cl])
			|| (a->getElement() == PTE[Element::Fe])
			//|| (a->getElement() == PTE[Element::Cn])
		 )
	{
		return true;
	}
	return false;
}

bool isPureProtein(std::vector<Atom*>& component)
{
	for (Position i=0; i<component.size(); ++i)
	{
		if  (component[i])
		{
			Atom* a = component[i];
			if (!isAminoAcid(a))
			{
				return false;
			}
		}
	}

	return true;
}

// works as follows:
//  - iterate over the chains of the protein
//  - for each chain, compute its pdb_id and chain_id
//  - with this, search in the PDB_to_BMRB table for the corresponding BMRB id / component_label
//  - read the BMRB file
//  - create a mapping from the alignment in the database
//  - get the chemical unit by the component_label
//  - iterate over all atoms and read the shift values from the ChemicalUnit->shifts
//
NMRStarFile::MolecularSystem::ChemicalUnit& getChemicalUnitByLabel(NMRStarFile& f, String const& label)
{
	NMRStarFile::MolecularSystem& mol_sys = f.getMolecularInformation();
	for (Position i=0; i<mol_sys.chemical_units.size(); ++i)
	{
		if (mol_sys.chemical_units[i].label == label)
			return mol_sys.chemical_units[i];
	}

	throw (Exception::OutOfRange(__FILE__, __LINE__));
}

std::set<String> readFeatureList(String feature_file, ofstream& log)
	throw(Exception::FileNotFound)
{
	std::set<String> features;

	LineBasedFile file(feature_file, std::ios::in);
	file.enableTrimWhitespaces(true);
	String line;

	while (file.readLine())
	{
		// skip the comment
		line = file.getLine();
		if (!line.hasPrefix("#"))
		{
			features.insert(file.getField(0));
			log << "   add feature " << file.getField(0)  << " to feature list" << std::endl;
		}
	}
	log << "   Total number of features to compute: " << features.size() << endl;
	file.close();
	return features;
}


//////////////////////////////////////
//
//
//                main 
//
//
//////////////////////////////////////

// prepared for galaxy
int main(int argc, char** argv)
{
	// instantiate CommandlineParser object supplying
	// - tool name
	// - short description
	// - version string
	// - build date
	// - category
	CommandlineParser parpars("ComputeNMRDescriptors", "computes descriptors for BALL's chemical shift prediction", "bla", String(__DATE__), "NMR");

	// we register an input file parameter 
	// - CLI switch
	// - description
	// - Inputfile
	// - required
	parpars.registerParameter("pdb_file", "input pdb-file", INFILE, true);
	parpars.setSupportedFormats("pdb_file", "pdb");

	parpars.registerParameter("bmrb_file", "input bmrb-file (only relevant for training)", INFILE, false);
	parpars.setSupportedFormats("bmrb_file", "str");

	parpars.registerParameter("feature_file", "input list of features to compute", INFILE, true, "features.txt");
	parpars.setSupportedFormats("feature_file", "txt");

	// we register an output file parameter 
	// - CLI switch
	// - description
	// - Outputfile
	// - required
	parpars.registerParameter("outfile", "output sqlite-file for descriptors", OUTFILE, true);
	parpars.setSupportedFormats("outfile", "db");

	//parpars.registerFlag("with_lig", "take ligand into account as well", false);
	//parpars.registerParameter("with_lig", "take ligand into account well", STRING, false);

	// register String parameter for supplying protein chain name
	parpars.registerParameter("prot_chain", "chain-id of the protein", STRING, false);

	// register String parameter for supplying ligand chain name
	parpars.registerParameter("lig_chain", "chain-name of the ligand", STRING, false);

	// register String parameter for supplying the pdb id
	parpars.registerParameter("pdb_id", "pdb id", STRING, false);

	// register String parameter for supplying the log file name
	parpars.registerParameter("log_file", "log file", STRING, false);

	// register String parameter for deciding between training a new model and evaluating an existing one
	parpars.registerParameter("app_type", "training or prediction", STRING, true, "prediction");
	list<String> app_types;
	app_types.push_back("prediction");
	app_types.push_back("training");
	parpars.setParameterRestrictions("app_type", app_types);

	String man = "This tool computes descriptors for NMR chemical shift prediction for a given pdb entry in pdb file format.\n\nOptional parameters are the bmrb file ('-bmrb') of the corresponding NMR data in NMRStar file format (which is only relevant for training), the chain id of the protein ('prot_chain'), the chain id of a ligand ('-lig_chain') if there is one, the PDB id of the protein ('-pdb_id'), and the application type ('app-type') indicating a prediciton or training scenario.\n\nOutput of this tool is an sqlite file containing NMR descriptors for each atom of supported chemical shift atom type.\n\nPlease cite the following: Andreas Hildebrandt, Anna Katharina Dehof, Alexander Rurainski, Andreas Bertsch, Marcel Schumann, Nora C Toussaint, Andreas Moll, Daniel Stockel, Stefan Nickels, Sabine C Mueller, Hans-Peter Lenhof, Oliver Kohlbacher: \"BALL - Biochemical Algorithms Library 1.3\", 2010, BMC Bioinformatics, 11:531";

	parpars.setToolManual(man);

	// here we set the types of I/O files
	parpars.setSupportedFormats("pdb_file","pdb");
	//parpars.setSupportedFormats("o","sqlite"); //TODO

	// parse the command line
	parpars.parse(argc, argv);

	Path path;

	// TODO: find a reasonable log file name
	// /local/anne/NMR/sql/logs/
	String logfilename = "ComputeNMRDescriptors.log";
	if (parpars.has("log_file"))
	{
		logfilename = parpars.get("log_file");
	}
	ofstream log(logfilename.c_str());
	//std::cout << " Logs can be found in " << logfilename << std::endl;

	// get the file names, etc
	String pdb_filename   = parpars.get("pdb_file");
	if (path.find(pdb_filename) == "")
	{
			log << "   Given PDB file " << pdb_filename << " does not exit." << endl;
			return 0;
	}

	String feature_file   = parpars.get("feature_file");
	if (path.find(feature_file) == "")
	{
			log << "   Given feature file " << feature_file << " does not exit." << endl;
			return 0;
	}

	String application_type = parpars.get("app_type");
	bool	 is_training      = application_type == "training";

	String bmrb_filename = "";

	if (parpars.has("bmrb_file"))
	{
		bmrb_filename = parpars.get("bmrb_file");
		std::cout << bmrb_filename  << endl;
		if (path.find(bmrb_filename) == "")
		{
			log << "   Given BMRB file " << bmrb_filename << " does not exit." << endl;
			return 0;
		}
	}
	if (path.find(bmrb_filename) == "" && is_training)
	{
		log << "   In application mode = training, a bmrb file is needed. Abort." << endl;
		return 0;
	}

	// misc 
	//TODO
	// do we have a ligand?
	bool with_ligand = false;
	String lig_chain;
	if (parpars.has("lig_chain"))
	{
		with_ligand = parpars.get("lig_chain").toBool();
		if (with_ligand)
			log  << "   Consider ligand with chain-id " << parpars.get("lig_chain") << "." << endl;
	}

	// 
	String pdb_id     = parpars.has("pdb_id") ? parpars.get("pdb_id") : "";
	String prot_chain = parpars.has("prot_chain") ? parpars.get("prot_chain"): "";


	// try to guess the PDB id if it was not supplied
	// e.g. 
	//  /local/anne/NMR/SHIFTX2_TEST/shiftx2-testset/A001_1KF3A.pdb 
	//  /local/anne/NMR/SHIFTX2_TEST/shiftx2-testset/A001_bmr4032.str.corr
	if (pdb_id == "")
	{
		std::vector<String> tmp;
		Size match = pdb_filename.split(tmp, "/");

		pdb_id = tmp[match-1];
		// do we have a ShiftX2 case with underscore?
		if (pdb_id.size()>9)
		{
			std::vector<String> tmp2;
			Size match2 = tmp[match-1].split(tmp2, "_");
			if (match2 > 0)
				pdb_id = tmp2[match2-1];
		}
		pdb_id = pdb_id.substr(0,4);
	}


	//
	// the real program
	//	

	// first read the reference pdb file
	BALL::PDBFile f_ref(pdb_filename);
	BALL::System S;
	f_ref >> S;

	log << "****\n ** PDBID: " <<  pdb_id << endl;

	// open a sqlite database
	String sqlite_file_name = parpars.get("outfile");

	// connect to the database
	SQLConnector connector(sqlite_file_name);

	//
	// prepare the pdb-file
	//

	// some preparing	
	BALL::FragmentDB frag_db("");
	Log.warn().disableOutput();
	S.apply(frag_db.normalize_names);
	S.apply(frag_db.add_hydrogens);
	S.apply(frag_db.build_bonds);
	Log.warn().enableOutput();

	// cut out and move the non-protein atoms into their own molecular object
	ConnectedComponentsProcessor ccp;
	S.apply(ccp);
	ConnectedComponentsProcessor::ComponentVector components = ccp.getComponents();

	Options gt_options;
	gt_options[GAFFTypeProcessor::Option::ATOMTYPE_FILENAME] = "atomtyping/GAFFTypes.dat";
	GAFFTypeProcessor gt(gt_options);
	AssignBondOrderProcessor abp;

	log << "Found " << components.size() << " connected components." << std::endl;
	for (Position i=0; i<components.size(); ++i)
	{
		// TODO: check consistency of user supplied chain information for ligand and protein
		log << "  Check component " << i << ":" << endl;
		if (!isPureProtein(components[i]))
		{
			// build new molecule, insert what is not protein component => done
			Molecule *m = new Molecule();
			//S.insert(*m);

			// do we have a mixed component with, e.g. bonded Ca 
			if (hasAminoAcid(components[i]))
			{
				log << "   mixed component " << endl;
				// if we have a single ION, delete its bonds and 
				// move the ION into the new molecule
				vector<Atom*> to_move;
				for (Position j=0; j<components[i].size(); ++j)
				{
					Atom* a = components[i][j];
					if (isIon(a))
					{
						to_move.push_back(a);
						components[i][j]=NULL;
					}
				}
				log << "   move " << to_move.size() << " ions" << endl;
				// move all ions
				for (Size j=0; j<to_move.size(); ++j)
				{
					Atom* a = to_move[j];
					// insert into the new molecule
					m->insert(*(a));

					// delete all former bonds 
					vector<Bond*> to_delete;
					for (Atom::BondIterator b_it = a->beginBond(); +b_it; ++b_it)
					{
						to_delete.push_back(&*b_it);
					}
					for (Size k = 0; k<to_delete.size(); ++k)
					{
						//TODO tut das? das Objekt Bond existiert vermutlich noch!
						to_delete[k]->clear();
					}
					log << "   !!!! WARNING !!!! : ion " << to_move[j]->getName() << " still has "
						<< to_move[j]->countBonds() << " bonds." << endl;
				}

				// final check if now we have a pure protein?
				if (!isPureProtein(components[i]))
				{
					log << "   !!!! WARNING !!!! : no ion cleanup was possible!! -->should abort TODO!!" << endl;
					for (Position k=0; k<components[i].size(); ++k)
					{
						Atom* a = components[i][k];
						if (!a) continue;
						if (!isAminoAcid(a))
						{
							log << "   !!!! ion cleanup failed for " << a->getFullName() << endl;
						}
					}
				}
				// such cases seem to crash the GAFFTyper! TODO: check :-)
				//  return 0;
			}
			else // : pure ligand
			{
				// move everything
				for (Position j=0; j<components[i].size(); ++j)
				{
					if (components[i][j])
						m->insert(*(components[i][j]));
					log << "   found a pure ligand with " << m->countAtoms() << "ats/" << m->countBonds()
						<< "bds" << endl;
				}
			}

			// did we move something into the new molecule at all?
			if (m->countAtoms() > 0)
			{
				S.insert(*m);

				// count the original bonds and hydrogens
				Size num_bonds = 0;
				Size num_H = 0;

				for (AtomIterator at_it = m->beginAtom(); +at_it; ++at_it)
				{
					if (at_it->getElement() == PTE[Element::H])
						num_H++;

					num_bonds += at_it->countBonds();
				}
				log << "   number of H's : " << num_H << endl;
				log << "   number of bonds : " << num_bonds/2. << endl;

				// apply the BondOrderAssigner and the GaffTyper
				m->apply(abp);
				log << "   Result BondOrderAssignment for ligand: #sol: " << abp.getNumberOfComputedSolutions()
					<< "  bond pen: " << abp.getTotalPenalty(0) << endl;

				log << "   atom type before gaff typer 1st atom: "
					<< m->beginAtom()->getProperty("atomtype").getString() << std::endl;

				m->apply(gt);

				log << "   atom type after gaff typer 1st atom: "
					<< m->beginAtom()->getProperty("atomtype").getString() << std::endl;
			}
		} //else pure protein --> nothing to do
		else
		{
			log << "  found pure protein" << endl;
		}
	}
	log << "  finished splitting" << endl;

	ResidueChecker rc(frag_db);
	S.apply(rc);

	BALL::HBondProcessor hbp;
	S.apply(hbp);

	BALL::SecondaryStructureProcessor ssp;
	S.apply(ssp);

	Path charge;
	String chargefilename= charge.find("charges/amber94.crg");

	// Please be quiet...
	Log.warn().disableOutput();
	BALL::AssignChargeProcessor chp(chargefilename);
	S.apply(chp);
	Log.warn().enableOutput();

	String chemical_unit_name;
	String pdb_alignment;
	String bmrb_alignment;

	// read and assign the NMR shifts if a file was given
	NMRStarFile nmr_file;
	if (is_training)
	{
		log << "read NMR File : " << bmrb_filename << endl;
		nmr_file.open(bmrb_filename);
		// don't remove this line, please! only the constructor implies read, but not open
		nmr_file.read();
	}

	// iterate over all chains of the protein
	Index chain_index = 0;
	for (ChainIterator chain_it = S.beginChain(); +chain_it; ++chain_it)
	{
		log << "consider chain " << chain_it->getName() << " " << chain_index
			<< " with seq " << Peptides::GetSequence(*chain_it) << endl;
		if (!chain_it->beginResidue()->isAminoAcid())
		{
			log << "Warning: skipping non-residue chain " << chain_it->getName() << " with first residue "
				<< chain_it->beginResidue()->getFullName() << endl;

			//				continue; //TODO
		}

		connector.query(&ballback, "BEGIN;");
		if (is_training)
		{
			connector.query(&ballback, "select BMRB_ChemUnit, Alignment_PDB, Alignment_BMRB from PDB_BMRB where PDB_ID=\'" + pdb_id
					+ "\' AND PDB_CHAIN_INDEX=\'" + String(chain_index) + "\';");

			// every chain should only appear once
			if (current_query_result.size() != 1)
			{
				log << "Warning: unexpected query result for " << pdb_id << " " << chain_it->getName()
					<< " " << chain_index << ":  aborting!" << endl;
				log << "Query result: " << std::endl;
				for (Size j=0; j < current_query_result.size(); j++)
				{
					log << "  * " << current_query_result[j] << std::endl;
				}
				continue;
			}

			// generate a mapping based on the alignment
			log << "create mapping bmrb -> chain ... "<< endl;

			vector<String> split;
			current_query_result[0].split(split, "|");

			if (split.size() != 3)
			{
				log << "Warning: something went wrong for " << pdb_id << " " << chain_it->getName()
					<< " aborting!" << std::endl;
				continue;
			}

			chemical_unit_name  = split[0];
			pdb_alignment  = split[1];
			bmrb_alignment = split[2];

			log << "create alignment mapping: " << endl;
			log << "ori pep seq " << Peptides::GetSequence(*chain_it) << endl;
			log << "alig pdb    " << pdb_alignment << endl;
			log << "alig bmr    " << bmrb_alignment << endl;
			log << "chemical unit name: " << chemical_unit_name << endl;

			NMRStarFile::BALLToBMRBMapper mapper(*chain_it, nmr_file, chemical_unit_name);
			mapper.createMapping(pdb_alignment, bmrb_alignment);


			// assign the chemical shifts as properties 
			log << "assign shifts ..." << endl;
			log << mapper.getBALLToBMRBMapping().size() << std::endl;
			nmr_file.assignShifts(mapper);
		}

		log << "precompute some structural properties ..." << endl;
		BALLPropertiesForShift propClass(&S, log);

		//
		// compute the shifts
		//
		log << "predict shifts ..." << endl;
		Path p;
		String inifilename = p.find("NMR/SHIFTX.ini");
		ShiftModel sm(inifilename);
		// restrict to aminoacids
		Selector aminoacids("residue(ALA) OR residue(ARG) OR residue(ASN) OR residue(ASP) OR residue(CYS) OR residue(GLU) OR residue(GLN) OR residue(GLY) OR residue(HIS) OR residue(ILE) OR residue(LEU) OR residue(LYS) OR residue(MET) OR residue(PHE) OR residue(PRO) OR residue(SER) OR residue(THR) OR residue(TRP) OR residue(TYR) OR residue(VAL)");
		//TODO
		// system or chain_it ??
		//chain_it->
		S.apply(aminoacids);
		//chain_it->
		S.apply(sm);
		//chain_it->
		S.deselect();

		log << "shift computation done." << endl;


		//
		// collect the structural properties
		//

		std::set<String>   tocompute = readFeatureList(feature_file, log);

		// check for special cases:
		// - rotamers
		// - ligand gaff types
		// - ligand gaff type distances
		// - close ligand het atoms 
		// - close ligand het atom distances
		if (tocompute.find("ROTAMERINDEX_") != tocompute.end())
		{
			// delete the key word
			tocompute.erase("ROTAMERINDEX_");

			// add the special amino acid types 
			String AAs[] = {"ALA","GLY","ARG","ASN","ASP","CYS","GLN","GLU","HIS","ILE","LEU",
				"LYS","MET","PHE","PRO","SER","THR","TRP","TYR","VAL"};
			for (Position i=0; i<20; i++)
			{
				Size j=0;
				Size num_rots = propClass.getRotamerLibrary()->getNumberOfRotamers(AAs[i]);
				if (num_rots > ROTAMER_MAX_IDX)
				{
					while (num_rots > ROTAMER_MAX_IDX)
					{
						num_rots = num_rots - ROTAMER_MAX_IDX;
						tocompute.insert(String("ROTAMERINDEX_")+AAs[i]+"_"+String(j));
						j++;
					}
				}
				else
				{
					tocompute.insert(String("ROTAMERINDEX_")+AAs[i]);
				}
			}
		}
		if (with_ligand)
		{
			tocompute.insert("NUM_HET_ATOMS");   // number of ligand atoms within 10 A distance 
			tocompute.insert("CL_HET_ATOM");     // atom element type of closest hetero atom
			tocompute.insert("CL_HET_GAFFType"); // GAFFtype of closest atom
			tocompute.insert("CL_HET_DIST");     // the distance to the closest HET atom

			if (tocompute.find("GAFFTYPE_") != tocompute.end())
			{
				// remove the key word
				tocompute.erase("GAFFTYPE_");

				// now the number of gaff types close
				std::set<String> gaff_types = gt.getTypeNames();
				int current_type = 0;
				for (std::set<String>::iterator type_it = gaff_types.begin(); type_it != gaff_types.end(); ++type_it)
				{
					tocompute.insert(String("GAFFTYPE_")+String(current_type)+"_"+*type_it);
					++current_type;
				}
			}

			if (tocompute.find("GAFFTYPE_DIST_") != tocompute.end())
			{
				tocompute.erase("GAFFTYPE_DIST_");

				// and the corresponding closest gaff type distances
				std::set<String> gaff_types = gt.getTypeNames();
				int current_type = 0;
				for (std::set<String>::iterator type_it = gaff_types.begin(); type_it != gaff_types.end(); ++type_it)
				{
					tocompute.insert(String("GAFFTYPE_DIST_")+String(current_type)+"_"+*type_it);
					++current_type;
				}
			}

			// now the number of elements close
			if (tocompute.find("HET_ELEMENT_") != tocompute.end())
			{
				tocompute.erase("HET_ELEMENT_");

				for (Size current_element = 1; current_element < BALL::Element::NUMBER_OF_ELEMENTS; ++current_element)
				{
					tocompute.insert(String("HET_ELEMENT_")+String(current_element)+"_"+ PTE[current_element].getSymbol());
				}
			}

			// and the corresponding closest element type distances
			if (tocompute.find("HET_ELEMENT_DIST_") != tocompute.end())
			{
				tocompute.erase("HET_ELEMENT_DIST_");

				for (Size current_element = 1; current_element < BALL::Element::NUMBER_OF_ELEMENTS; ++current_element)
				{
					tocompute.insert(String("HET_ELEMENT_DIST_")+String(current_element)+"_"+PTE[current_element].getSymbol());
				}
			}
		} // end case ligand

		//
		//    create the table
		//
		// These are the necessary identifieres!
		String atom_columns = String("PDB_ID char(4), CHAIN_ID char(1), RES_ID int, ATOM_NAME char(8)");
		// ELEMENT char(2),");

		for (std::set<String>::iterator it = tocompute.begin(); it != tocompute.end(); it++)
		{
			atom_columns += "," + *it + " " + propClass.getType(*it);
		}

		log << "new table definition: " << atom_columns << endl;

		connector.createTable("ATOM_PROPS", atom_columns);
		connector.query(&ballback, "create index if not exists idx on ATOM_PROPS (PDB_ID, CHAIN_ID);");

		//
		//  get NMR properties
		//
		String pH = "NA";
		String temperature  = "NA";
		String pressure     = "NA";
		String h_nmr_solution = "NA";
		String c_nmr_solution = "NA";
		String n_nmr_solution = "NA";

		if (is_training)
		{
			log << "compute NMR properties..." << endl;

			// use the chemical unit to find the corresponding SampleCondition
			NMRStarFile::MolecularSystem::ChemicalUnit chemical_unit = getChemicalUnitByLabel(nmr_file, chemical_unit_name);

			log << "chemical unit = " << chemical_unit_name << endl;
			if (   (    tocompute.find("PH")    != tocompute.end() 
						|| tocompute.find("TEMP")  != tocompute.end() 
						|| tocompute.find("PRESS") != tocompute.end()) 
					&& chemical_unit.shifts 
					&& nmr_file.hasSampleCondition(chemical_unit.shifts->condition))
			{
				NMRStarFile::SampleCondition& condition = nmr_file.getSampleConditionByName(chemical_unit.shifts->condition);
				if (condition.hasType("pH"))
					pH = condition.values["pH"];
				if (condition.hasType("pH*"))
					pH = condition.values["pH*"];
				if (condition.hasType("temperature"))
					temperature  = condition.values["temperature"];
				if (condition.hasType("pressure"))
					pressure  = condition.values["pressure"];

				log << "NMR conditions: " << pH << "--" << temperature
					<< "--" << pressure << endl;
			}
			else
			{
				log <<  "   WARNING: no sample condition " << chemical_unit.shifts->condition << " found!" << std::endl;
			}

			if (    tocompute.find("H_NMR_SOLUTION") != tocompute.end() 
					|| tocompute.find("C_NMR_SOLUTION")  != tocompute.end()
					|| tocompute.find("N_NMR_SOLUTION")  != tocompute.end()) 
			{ 
				if (chemical_unit.shifts && nmr_file.hasShiftReferenceSet(chemical_unit.shifts->reference))
				{
					NMRStarFile::ShiftReferenceSet& shift_reference_set = nmr_file.getShiftReferenceSetByName(chemical_unit.shifts->reference);

					for (Size k=0; k < shift_reference_set.elements.size(); k++)
					{
						if (shift_reference_set.elements[k].atom_type == "H")
							h_nmr_solution = shift_reference_set.elements[k].mol_common_name;
						if (shift_reference_set.elements[k].atom_type == "C")
							c_nmr_solution = shift_reference_set.elements[k].mol_common_name;
						if (shift_reference_set.elements[k].atom_type == "N")
							n_nmr_solution = shift_reference_set.elements[k].mol_common_name;
					}
					log << "NMR solution (H/C/N): " << h_nmr_solution << "/" << c_nmr_solution << "/" << n_nmr_solution << endl;
				}
				else
				{
					log <<  "   !!!! WARNING !!!! : no shift reference set " << chemical_unit.shifts->reference 
						<< " found!" << std::endl;
				}
			}

			log << "Assigned " << nmr_file.getNumberOfShiftsAssigned() << std::endl;
		}

		//
		//     collect all properties	
		//
		bool found_shift = false;
		for (ResidueIterator res_it = S.beginResidue(); +res_it; ++res_it)
		{
			for (PDBAtomIterator at_it = res_it->beginPDBAtom(); +at_it; ++at_it)
			{
				if (    (!is_training || at_it->getProperty(BALL::ShiftModule::PROPERTY__EXPERIMENTAL__SHIFT).getFloat()!=0)
						//   && (at_it->getProperty(BALL::ShiftModule::PROPERTY__SHIFT).getFloat()!=0))
					   && (at_it->getProperty(BALL::RandomCoilShiftProcessor::PROPERTY__RANDOM_COIL_SHIFT).getFloat() != 0))
				{
					found_shift = true;

					// -->compute the structural properties for each atom
					propClass.computeProperties(&*at_it , tocompute);

					// -->collect the properties values
					//log<< at_it->getVelocity()<<endl;
					std::vector<String> row_names;
					std::vector<String> row_vector;

					row_names.push_back("PDB_ID");
					row_vector.push_back(String(pdb_id));

					row_names.push_back("CHAIN_ID");
					row_vector.push_back(String(at_it->getResidue()->getChain()->getName()));

					row_names.push_back("RES_ID");
					row_vector.push_back(String(at_it->getResidue()->getID()));

					row_names.push_back("ATOM_NAME");
					row_vector.push_back(at_it->getName());


					for (std::set<String>::iterator it = tocompute.begin(); it != tocompute.end(); it++)
					{
					  if (*it == "PH")
					  {
							row_names.push_back("PH");
							row_vector.push_back(pH);
					  }
					  else if (*it == "TEMP")
					  {
							row_names.push_back("TEMP");
							row_vector.push_back(temperature);
					  }
					  else if (*it == "PRESS")
					  {
							row_names.push_back("PRESS");
							row_vector.push_back(pressure);
					  }
					  else if (*it == "H_NMR_SOLUTION")
					  {
							row_names.push_back("H_NMR_SOLUTION");
							row_vector.push_back(h_nmr_solution);
					  }
					  else if (*it == "C_NMR_SOLUTION")
					  {
							row_names.push_back("C_NMR_SOLUTION");
							row_vector.push_back(c_nmr_solution);
					  }
					  else if (*it == "N_NMR_SOLUTION")
					  {
							row_names.push_back("N_NMR_SOLUTION");
							row_vector.push_back(n_nmr_solution);
					  }
					  else
						{
							// now the real features
							String prop = propClass[*it];
							if (prop == "NA")
								row_vector.push_back("NULL");
							else
								row_vector.push_back(prop);
							row_names.push_back(*it);
						}
					}

					/*					log << " \n\n************** \n";
					 					log << "Insert values: \n";
					 					for (Position i=0; i<row_vector.size(); ++i)
					 					log << row_names[i] + ":" + row_vector[i] << ", " << endl;
					 					*/

					bool result = connector.insertValues("ATOM_PROPS", row_names, row_vector);
					if (!result)
					{
					  log << "Inserting failed" << std::endl;
					  Log.info() << "failed query was ";
					  for (Position i=0; i<row_vector.size(); ++i)
							Log.info() << row_names[i] + ":" + row_vector[i] << ", " << " ";
					}
				}
				else if (at_it->getProperty(BALL::ShiftModule::PROPERTY__EXPERIMENTAL__SHIFT).getFloat() != 0)
				{
					log << "Skip atom " << at_it->getFullName() << " with experimental shift value "
							<< at_it->getProperty(BALL::ShiftModule::PROPERTY__EXPERIMENTAL__SHIFT).getFloat();
					Log.info() << "Skip atom " << at_it->getFullName() << " with experimental shift value "
										 << at_it->getProperty(BALL::ShiftModule::PROPERTY__EXPERIMENTAL__SHIFT).getFloat();
					if (at_it->getProperty(BALL::RandomCoilShiftProcessor::PROPERTY__RANDOM_COIL_SHIFT).getFloat() == 0)
					{
						log << " since random coil is zero";
						Log.info() << " since random coil is zero";
					}
					log << std::endl;
					Log.info() << std::endl;
				}
			}
		}
		if (!found_shift)
		{
			log << "   !!!! WARNING !!!! : No shifts computed for chain " << chain_it->getName() << " " << chain_index << "!" << endl;
		}
		chain_index++;
	}

	connector.query(&ballback, "COMMIT;");
}
