#!/usr/bin/python

import sys, os
import sqlite3
import urllib2, urllib, xml.dom.minidom
import re
from BALL import *
import pprint

from nmr_definitions import *

ref_home = bmrbhome_refdb+"/bmr%s_corr.str"

#####
def addColumn(colname, conn):
  try:
    conn.execute(("alter table ATOM_PROPS add column %s REAL default -1.0" % colname))
  except:
    print ("%s column already there" % colname)

  return  


#####
def addValues(result_file, bmrb_unit, pdb_id, chain_id, chain_index, colname):
  print "add values for", colname, "from file", result_file,
  # add shifts
  pdb_file = pdbhome + "/" + pdb_id + ".pdb"
  print pdb_file, result_file
  if (os.path.exists(result_file) and os.path.exists(pdb_file)):
    pdbfile = PDBFile(str(pdb_file), OpenMode(File.MODE_IN))
    #pdbfile.open(str(pdb_file))#, OpenMode(File.MODE_IN))
    S = System()
    pdbfile.read(S)
    print S.countChains(), chain_index
    current_chain_id = 0
    fdb = FragmentDB("")
    S.apply(fdb.normalize_names)
    #if (S.countChains()>chain_index):
      #chain = S.getChain(chain_index)
    for chain in chains(S):
      if chain_index == current_chain_id:
        # read the NMRStar file
        nmr_file = NMRStarFile(str(result_file), OpenMode(File.MODE_IN))
        nmr_file.read()
      
        # compute an alignment
        pdb_alignment = Peptides.GetSequence(chain)
        try:
          nmr_alignment = nmr_file.getResidueSequence(current_chain_id)
        except e:
          print "could not get nmr alignment"
        print pdb_alignment, "++"
        print nmr_alignment, "++"
        mapper = NMRStarFile.BALLToBMRBMapper(chain, nmr_file, String(bmrb_unit))
        print "Creating mapping"
        valid = True
        valid &= mapper.createMapping(pdb_alignment, nmr_alignment)
        print "Done"
        if (not valid):
          print "Creation of mapping failed"
          continue
        valid &= nmr_file.assignShifts(mapper)
        if (not valid):
          print "Shift assignment failed"
          continue
        num_ass = nmr_file.getNumberOfShiftsAssigned()
        for atom in atoms(chain):
          if (atom.hasProperty(ShiftModule.PROPERTY__EXPERIMENTAL__SHIFT)):
            #print atom.getFullName(), " has shift ", atom.getProperty(ShiftModule.PROPERTY__EXPERIMENTAL__SHIFT).getFloat() 
            atom_name  = str(atom.getFullName()).split(":")[1]
            value = atom.getProperty(ShiftModule.PROPERTY__EXPERIMENTAL__SHIFT).getFloat()
            residue_index = atom.getResidue().getID()
            print colname, value, pdb_id, chain_id, atom_name, residue_index
            try:
              c2.execute("update ATOM_PROPS set %s=\'%s\' where PDB_ID=\'%s\' and CHAIN_ID=\'%s\' and ATOM_NAME=\'%s\' and RES_ID=\'%s\'" % (colname, value, pdb_id, chain_id, atom_name, residue_index))
              text = ("update ATOM_PROPS set %s=\'%s\' where PDB_ID=\'%s\' and CHAIN_ID=\'%s\' and ATOM_NAME=\'%s\' and RES_ID=\'%s\'" % (colname, value, pdb_id, chain_id, atom_name, residue_index))
              print "." ,
              #print "." , text
            except:
              print "no update for atom ", atom_name
      #else:
      #print "Invalid chain index", chain_index
      current_chain_id = current_chain_id + 1



#####################################
#
#       main
#
#####################################

# connect to the db
print "Connecting to DB " + nmr_sqlite_db
conn = sqlite3.connect(nmr_sqlite_db)

# and add a column for the ShiftX2 values
addColumn("RefDB", conn)

# and read the mapping table
c  = conn.cursor()
c2 = conn.cursor()
c.execute("select PDB_ID, PDB_CHAIN_ID, PDB_CHAIN_INDEX, BMRB_ID, BMRB_ChemUnit from PDB_BMRB")

for row in c:
  try:
    pdb_id = row[0]
    print pdb_id, 
    # We use this to jump over 1JNJ
    if (len(sys.argv) > 1) and pdb_id in sys.argv[1:]:
      continue
    chain_id    = row[1]
    print chain_id,
    chain_index = row[2]
    print chain_index, "++"
    bmrb_index  = row[3]
    bmrb_unit  = row[4]
    if (chain_id != ""):
      ref_result_file = ref_home %(bmrb_index)
      if (os.path.exists(ref_result_file)): 
        addValues(ref_result_file, bmrb_unit, pdb_id, chain_id, chain_index, "RefDB")
      else:
        print "Missing file", ref_result_file
    elif chain_id != "":
      print "No ChainID given!"
    conn.commit()
  except Exception as e:
    print e
