#!/bin/bash

#
#		NightShift: NMR shift inference by General Hybrid model Training 
#		 --- A pipeline for automated NMR shift prediction training ---
#		
#		Anna Dehof
#

# remember the directory from which we were called
olddir=$(pwd)

# change into our pipeline base directory
cd $(dirname $0)

source ./NMR_pipeline_definitions.sh

export PATH=$(readlink -f ../build):$PATH

cleanup_db=true         		# set to true if the data base should be cleaned before the pipeline run
#download_data=true        	# obtain current data from PDB and BMRB
download_data=false        	# obtain current data from PDB and BMRB
create_pdb_bmrb_table=true #true 	# create pdb to bmrb mapping (table PDB_BMRB)
#create_pdb_bmrb_table=false #true 	# create pdb to bmrb mapping (table PDB_BMRB)
restrict_homology=true #true      # use homology filter (directly while downloading the data or afterwards)
compute_descriptors=true    # create and fill ATOM_PROPS table
#compute_descriptors=false    # create and fill ATOM_PROPS table
add_ShiftX2=true           # run ShiftX2 and add values to the data set
#cleanup_atom_props=true  	  # set to true to remove all data from ATOM_PROPS table to recompute all properties
cleanup_atom_props=false  	  # set to true to remove all data from ATOM_PROPS table to recompute all properties
download_refdb_data=true 	# set to true if reference corrected versions should be downloaded from the RefDB
train_from_refdb=true 	# use refdb data as input for training and evaluation
train_rf_model=true         # set to true to train and evaluate a random forest model
train_rf_sig_model=false    # set to true to train a second random forest model based on the significant features of the last run
train_linear_model=false    # set to true to train and evaluate a linear model

# TODO do we really need both options compute_descriptors and cleanup_atom_props?? TODO


mkdir $datadir      2> /dev/null
mkdir $resultsdir   2> /dev/null
mkdir $logdir       2> /dev/null

## handle the protein-ligand case
if [ "$prediction_type" = "ProteinLigand" ]; then
	mappingfilename=${mappingfilename_ligand}
	nmr_sqlite_db=${nmr_sqlite_db_ligand}
	bmrbhome=${bmrbhome_ligand}
	pdbhome=${pdbhome_ligand}
	feature_file=${feature_file_ligand}
fi

#mappingfilename="/home/HPL/anne/Desktop/mapping.txt"      # only 20 pairs!

#mappingfilename="/home/HPL/anne/Desktop/mapping-galalxy_test.txt"  
#features="/home/HPL/anne/ballaxy/database/files/000/dataset_132.dat"
#nmr_sqlite_db="/home/HPL/anne/DEVELOP/GALAXY/NMR/testAAA.sqlite"

if ${debug}; then	
	## for debug
	echo "----------------------------------"
	echo "           pipeline.sh            "
	echo "----------------------------------"
	echo "mapping_file: ${mappingfilename}"
	echo "DB : ${nmr_sqlite_db}"
	echo "BMRB: ${bmrbhome}"
	echo "PDB: ${pdbhome}"
	echo "data: ${datadir}"
	echo "results: ${resultsdir}"
	echo "pred_type=${prediction_type}"
	echo "num_proc=${num_filldb_procs}"
	echo "feature_file=${feature_file}" 
	echo "logs:${logdir}"
	echo "----------------------------------"
fi

if ${cleanup_db}; then	
	echo "**************************************************************************"
	echo "  Cleaning up old data base..."	
	echo "**************************************************************************"
	rm $nmr_sqlite_db 2> /dev/null
fi

if ${download_data}; then
	echo "**************************************************************************"
	echo "  Download data... (this may take a while...)"
	echo "     (see ${logdir}download_pdb_and_bmrb.log)"
	echo "     (find mappings in ${mappingfilename} etc.)"
	echo "**************************************************************************"
	python getPDBandBMRBFiles.py --mappingfilename=${mappingfilename_protein} \
	                             --mappingfilename_ligand=${mappingfilename_ligand} \
															 --mappingfilename_dna=${mappingfilename_dna} \
															 --mappingfilename_dna_ligand=${mappingfilename_dna_ligand} \
														   > ${logdir}download_pdb_and_bmrb.log 2>&1
	## one may directly restric the mapping by homology:
	if ${restrict_homology}; then
		python storeHomologyInformation.py \
		     	--mappingfile=${mappingfilename} \
					--pisces_maxpc=${pisces_maxpc} > ${logdir}store_homology_information.log 2>&1
	fi
fi

if ${create_pdb_bmrb_table}; then
	echo "**************************************************************************"
	echo "  Create PDB to BMRB table..."
	echo "      (see ${logdir}create_pdb_bmrb_table.log)"
	echo "      (find DB in ${nmr_sqlite_db})"
	echo "**************************************************************************"	
	sqlite3 ${nmr_sqlite_db} "drop table if exists PDB_BMRB;"
	python createPDB_BMRBTable.py --DB=${nmr_sqlite_db} \
	                              --mappingfile=${mappingfilename} \
															  > ${logdir}create_pdb_bmrb_table.log 2>&1    

fi

if ${restrict_homology}; then
	if [ -n ${pisces_binary:-""} ]; then 
		echo "**************************************************************************"
		echo "  Add a homology filter to the PDB to BMRB table..."
		echo "      (see ${logdir}store_homology_information.log)" 
		echo "**************************************************************************"
		## test for presence of pisces binary
		if [ -x ${pisces_binary} ]; then
			homology_column_name="NON_REDUNDANT_${pisces_maxpc}"
			python storeHomologyInformation.py --mappingfile=${mappingfilename} \
			                                   --DB=${nmr_sqlite_db} \
 																				 --colname=${homology_column_name} \
																				 --pisces_maxpc=${pisces_maxpc} \
																				 > ${logdir}store_homology_information.log 2>&1
		else
			echo "Could not execute pisces binary at ${pisces_path}"
		fi
	fi
fi

if ${compute_descriptors}; then
	echo "**************************************************************************"
	echo "  Create PDB atom to BMRB atom table... (this may take a while...)"
	echo "      (see ${logdir}sql/\*)" 
	echo "      (find DB in ${nmr_sqlite_db})"
	echo "**************************************************************************"
	if ${cleanup_atom_props}; then
		sqlite3 ${nmr_sqlite_db} "drop table if exists ATOM_PROPS;"
	fi
	if [ "$prediction_type" = "ProteinLigand" ]; then
		tmp_feature_file="$datadir/features_all.txt"
		cat $feature_file_protein $feature_file_ligand > $tmp_feature_file
		feature_file=$tmp_feature_file
	fi

	./fillNMRDBParallel.sh --db=${nmr_sqlite_db} \
	                       --pred_type=${prediction_type} \
												 --num_proc=${num_filldb_procs} \
	                       --feature_file=${feature_file} \
												 --mapping_file=${mappingfilename} \
												 --bmrb_home=${bmrbhome} \
												 --pdb_home=${pdbhome} 
fi

if  ${add_ShiftX2}; then
	if [ -n ${shiftx2_path:-""} ]; then 
  	echo "************************************************************************"
  	echo "  Call ShiftX2... (this may take a while...)"
		echo "      (see ${logdir}add_shiftx2_values.log)" 
  	echo "************************************************************************"
		python ${source_dir}/addShiftX2Values.py --DB=${nmr_sqlite_db} \
		                                         --SHIFTX2HOME=${shiftx2_path} \
																						 --PDBHOME=${pdbhome} \
																						 --SHIFTX2RESULTS=${shiftx2_results_path} \
		                   > ${logdir}addShiftX2Values.log 2>&1

	fi	
fi

if ${download_refdb_data}; then
  echo "************************************************************************"
  echo "  Download data from RefDB... (this may take a while...)"
	echo "      (see ${logdir}refdb_download.log)" 
  echo "************************************************************************"
	mkdir ${bmrbhome_refdb}
	python ./downloadFromRefDB.py > ${logdir}refdb_download.log 2>&1 
  python ./addRefDBShifts.py >> ${logdir}refdb_download.log 2>&1
fi

if ${train_rf_model}; then
	USE_REFDB="" 
	if ${train_from_refdb}; then
		USE_REFDB="-use_refdb=true"
	fi
	Rmodel="${resultsdir}Protein_RandomForestModel.Rdata" 
  echo "************************************************************************"
  echo "  Training a random forest model... (this may take a while...)"
	echo "      (see ${logdir}train_rf_model.log)" 
 	echo "  The resulting model will be placed in:" 
	echo "      ${Rmodel}" 
	echo "  The evaluation will be placed in:"
  echo "      ${resultsdir}evaluation_rf_model.pdf" 
	echo "      ${resultsdir}evaluation_rf_model_features.pdf"
  echo "************************************************************************"
	./create_models_modular.R -training_db=${nmr_sqlite_db} ${USE_REFDB}\
	                         	-use_random_forest=true  \
														-model_name=${Rmodel} \
														-train_size=0.6 \
													 	-feature_file=${feature_file} > ${logdir}train_rf_model.log 2>&1

	python extract_results_for_tex.py ${logdir}train_rf_model.log > ${resultsdir}evaluation_rf_model.tex   2> /dev/null
	python extract_features.py ${logdir}train_rf_model.log ${feature_sig_cutoff} ${results}features_sig.txt > ${resultsdir}evaluation_rf_model_features.tex 2> /dev/null
	cd ${resultsdir}
	pdflatex evaluation_rf_model.tex > /dev/null 2>&1
	pdflatex evaluation_rf_model_features.tex > /dev/null 2>&1
	cd -
	##copy all
fi

if ${train_rf_sig_model}; then
	Rmodel="${resultsdir}Protein_RandomForestModel_sig.Rdata" 
  echo "************************************************************************"
	echo "  Training a random forest model on reduced feature set ${resultsdir}features_sig.txt (cutoff: ${feature_sig_cutoff})... (this may take a while...)"
	echo "      (see ${logdir}train_rf_model_sig.log)" 
 	echo "  The resulting model will be placed in:" 
	echo "      ${Rmodel}" 
	echo "  The evaluation will be placed in:"
  echo "      ${resultsdir}evaluation_rf_model_sig.pdf" 
	echo "      ${resultsdir}evaluation_rf_model_features_sig.pdf"
  echo "************************************************************************"	
	python extract_features.py ${logdir}train_rf_model.log ${feature_sig_cutoff} ${results}features_sig.txt > ${resultsdir}evaluation_rf_model_features.tex 2> /dev/null

	./create_models_modular.R -training_db=${nmr_sqlite_db} \
	                         	-use_random_forest=true  \
														-model_name=${Rmodel} \
														-train_size=0.6 \
													 	-feature_file=${resultsdir}features_sig.txt > ${logdir}train_rf_model_sig.log 2>&1

	python extract_results_for_tex.py ${logdir}train_rf_model_sig.log > ${resultsdir}evaluation_rf_model_sig.tex   2> /dev/null
	python extract_features.py ${logdir}train_rf_model_sig.log ${feature_sig_cutoff} ${resultsdir}features_sig2.txt > ${resultsdir}evaluation_rf_model_features_sig.tex 2> /dev/null
	cd ${resultsdir}
	pdflatex evaluation_rf_model_sig.tex > /dev/null 2>&1
	pdflatex evaluation_rf_model_features_sig.tex > /dev/null 2>&1
	cd -
	##copy all
fi

if ${train_linear_model}; then
	# TODO!!
	Rmodel="${resultsdir}Protein_LineareModel.Rdata" 
  echo "************************************************************************"
  echo "  Training a linear model... (this may take a while...)"
	echo "      (see ${logdir}train_linear_model.log)"  	
	echo "  The resulting model will be placed in:" 
	echo "      ${Rmodel}" 
	echo "  The evaluation will be placed in:"
  echo "      ${resultsdir}evaluation_linear_model.pdf"
  echo "************************************************************************"	
	./create_models_modular.R ${nmr_sqlite_db} > ${logdir}train_linear_model.log 2>&1
	python extract_results_for_tex.py ${logdir}train_linear_model.log > ${resultsdir}evaluation_linear_model.tex 2> /dev/null
	cd ${resultsdir}
	pdflatex evaluation_linear_model.tex > /dev/null 2>&1
	cd -
fi

# go back
cd ${olddir}

echo " Done."
# done.
