#include <BALL/DATATYPE/string.h>

#include <cstdio>
#include <stdio.h>
#include <iostream>
#include <sqlite3.h>


namespace BALL
{
	class SQLConnector
	{
		public:
			SQLConnector(String database_name);
			virtual ~SQLConnector();

			bool isValid();

			bool SQLCheckResult();

			bool createTable(String table_name, String columns);
			bool dropTable(String table_name);

			bool insertValues(String table_name, String values); 
			bool insertValues(String table_name, std::vector<String> const& values);
			bool insertValues(String table_name, std::vector<String> const& row_names, std::vector<String> const& values);
			bool query(String query);

			bool query(int(*new_callback)(void*, int, char**, char**), String query);

			static int callback(void *NotUsed, int argc, char** argv, char **azColName);

		private:
			sqlite3 *db_;
			char *zErrMsg_;
			int rc_;
			bool is_valid_;

			String database_name_;
	};
}
