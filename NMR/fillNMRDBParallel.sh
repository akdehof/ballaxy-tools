#!/bin/bash

#
#		NightShift: NMR shift inference by General Hybrid model Training 
#		 --- A pipeline for automated NMR shift prediction training ---
#		
#		Anna Dehof
#

debug=false

# explain how to use the script
NO_ARGS=0 
E_OPTERROR=85
if [ $# -eq "$NO_ARGS" ]    # Script invoked with no command-line args?
then
  echo "Usage: `basename $0` options (--db=... --outfile=... --predtype=[PureProtein|ProteinLigand]  --mapping_file=...)" #, --num_proc=...)"
  exit $E_OPTERROR          # Exit and explain usage.
                            # Usage: scriptname -options
                            # Note: dash (-) necessary
fi

export PATH=${source_dir}/../build:${PATH}

# parse the command line arguments
while getopts ":-:" Option; do
  case $Option in
		- )
			case "${OPTARG}" in
				db=*)
					export nmr_sqlite_db=${OPTARG#*=}
				  ;;
				outfile=*)
					export nmr_sqlite_db_out=${OPTARG#*=}	
          ;;
        pred_type=*)
					export prediction_type=${OPTARG#*=}
				  ;;
        num_proc=*)
					export tmp_num_procs=${OPTARG#*=}
					;;
				mapping_file=*)
					export mapping=${OPTARG#*=}
					;;
				bmrb_home=*)
					export bmrb_home=${OPTARG#*=}
					;;
				pdb_home=*)
					export pdb_home=${OPTARG#*=}
					;;
				feature_file=*)
					export feature_file=${OPTARG#*=}
					;;

			esac
  esac
done

# check command line arguments
if [ "${nmr_sqlite_db}" = "" ]; then
  echo "Error: invalid nmr prediction db!"
  exit $E_OPTERROR  
fi

if [ "${prediction_type}" = "" ]; then
  echo "Error: invalid prediction type!"
  exit $E_OPTERROR  
fi

# TODO: keep num_proc fixed or editable?
if [ ! -z ${tmp_num_procs} ]; then
  export num_filldb_procs=${tmp_num_procs}
else
  echo ${tmp_num_procs}
fi

# if no outfile is specified pipe the given db
if [ "${nmr_sqlite_db_out}" = "" ]; then
  nmr_sqlite_db_out=${nmr_sqlite_db}
else
  # TODO test!
  cp ${nmr_sqlite_db}=${nmr_sqlite_db_out}
fi

if ${debug}; then	
	#for debugging
	echo "----------------------"
	echo "    fillNMRDBParallel.sh "
	echo "----------------------"
	echo "Chosen options:"
	echo "  nmr_sqlite_db", $nmr_sqlite_db
	echo "  nmr_sqlite_db_out", $nmr_sqlite_db_out
	echo "  num_filldb_procs", $num_filldb_procs
	echo "  prediction type", $prediction_type	
	echo "  feature_file", $feature_file
	echo "  mappingfile", $mapping
	echo "  pdb_home", $pdb_home
	echo "  bmrb_home", $bmrb_home
	echo "----------------------"
fi


####################################################
#
#                  let's start
#
####################################################
cd $datadir

sql_logdir=${logdir}/sql

mkdir -p ${sql_logdir} 2> /dev/null

# TODO get rid of dependency from mapping file

# TODO get pred_type aus dem Header des mappingfile

# TODO: get rid of this variable LIGAND
LIGAND=
if [ "$prediction_type" == "ProteinLigand" ]; then
  LIGAND="LIGAND"
fi
#echo $LIGAND
# we need a temporary directory for our parallel processes
workdir=${datadir}/$$
mkdir -p ${workdir}/logs/sql # create all required directories

# start the different processes
let last_proc=${num_filldb_procs}-1
for num_procs in $(seq 0 $last_proc); do	
	# delete the old log file
	rm ${sql_logdir}/run_parallel_out_${num_procs} 2> /dev/null
	
	if ${debug}; then	
		## for debugging
		echo "-----------------------"
		echo " fillNMRDBParallel.sh:"
		echo "-----------------------"
		echo "--num_procs=$num_procs"
		echo "--max_procs=$num_filldb_procs"
		echo "--mapping_file=${mapping}"  
		echo "--db_name=$nmr_sqlite_db"
		echo "--pred_type=${prediction_type}"
		echo "--feature_file=${feature_file}"
		echo "--bmrb_home=${bmrb_home}"
		echo "--pdb_home=${pdb_home}"
		echo "--workdir=${workdir}"
		echo "LIGAND", ${LIGAND}
		echo "sql_logdir", ${sql_logdir}
		echo "source_dir", ${source_dir}
		echo "-----------------------"
	fi

	debugflag=""
	if ${debug}; then
		debugflag="--debug=1"
	fi
	python ${source_dir}/run_parallel.py --num_procs=${num_procs} \
																	 	 --max_procs=${num_filldb_procs} \
																		 --mapping_file=${mapping}  \
																		 --db_name=${nmr_sqlite_db} \
																		 --feature_file=${feature_file} \
																		 --pred_type=${prediction_type} \
																		 --bmrb_home=${bmrb_home} \
																		 --pdb_home=${pdb_home} \
																		 --workdir=${workdir} \
																		 ${debugflag} \
						                         > ${sql_logdir}/run_parallel_out_${num_procs} 2>&1 &
done

wait

# join the created tables
for num_procs in $(seq 0 $last_proc); do
	sqlite3 ${workdir}/nmr_parallel_${num_procs}.db '.dump "ATOM_PROPS"' > ${workdir}/nmr_parallel_${num_procs}_dump.sql
	sqlite3 ${nmr_sqlite_db} ".read ${workdir}/nmr_parallel_${num_procs}_dump.sql"
done

# remove all temporary files
rm -rf ${workdir}

# create index on the ATOM_PROP table
sqlite3 ${nmr_sqlite_db} "create index if not exists idx on ATOM_PROPS (PDB_ID, CHAIN_ID)"

# come back
cd $source_dir
