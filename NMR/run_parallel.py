#!/usr/bin/python
#
#		NightShift: NMR shift inference by General Hybrid model Training 
#		 --- A pipeline for automated NMR shift prediction training ---
#		
#		Anna Dehof
#

import os, os.path, sys, shutil
from nmr_definitions import *
import sqlite3

ligand = False


####################################################
##
##
##              MAIN
##
##
####################################################
# give a usage hint
if ((len(sys.argv) < 2)): # or (len(sys.argv) > 6)): 
  print "Usage: %s --num_procs=... --max_procs=... --db_name=... --mapping_file=... --feature_file=... -pred_type=[PureProtein|ProteinLigand] --workdir=... (--pdb_home=... --bmrb_home=...) (--debug=1)" % sys.argv[0] 
  #print "Usage: %s num_procs max_procs db_name (\"\"|LIGAND)" % sys.argv[0] 
  exit (1)

# parse the command line arguments
command_args = {}
for arg in sys.argv[1:]:
  sp = arg.split("=")
  command_args[sp[0][2:]] = sp[1].replace("\"", "")

#print command_args

#
#    check arguments
#

# --mappingfile=...
if not command_args.has_key("mapping_file"):
  print("Error: invalid mapping file!")
  exit (1)

mappingfilename = command_args["mapping_file"]
if not (os.path.isfile(mappingfilename)):
  print("Error: invalid mapping file", mappingfilename, "!")
  exit (1)

# --pred_type==[PureProtein|ProteinLigand]"
if command_args.has_key("pred_type"):
  if (command_args["pred_type"]=="ProteinLigand"):
    ligand = True
  elif (command_args["pred_type"]=="PureProtein"):
    ligand = False
  else: 
    print("Error: invalid prediction type!")
    exit (1)

#--db_name=...
if not command_args.has_key("db_name"): 
  print("Error: missing db file!")
  exit (1)

# check if db exists
db_name = command_args["db_name"]
if not (os.path.isfile(db_name)):
  print("Error: invalid db file", db_name , "!")
  exit (1)

#--feature_file=...
if not command_args.has_key("feature_file"): 
  print("Error: missing feature file!")
  exit (1)
featurefilename = command_args["feature_file"]


## connect to our database and ask for the number of mappings
## we currently solve this by taking the mapping file
#conn = sqlite3.connect(db_name)
## create a cursor
#c = conn.cursor()
#c.execute("select count(PDB_ID) from PDB_BMRB")
#num_mappings = c.fetchone()
#print num_mappings
#if not (num_mappings):
#  print("Error: invalid db ", db_name , "!")
#  exit (1)

#--num_procs=... 
#--max_procs=... 
if (not (command_args.has_key("num_procs") and command_args.has_key("max_procs"))):
  print("Error: processor ids are missing!")
  exit (1)

my_id       = int(command_args["num_procs"])
num_threads = int(command_args["max_procs"])
if ((my_id<0) or (num_threads<0) or (num_threads<=my_id) or (my_id>10)): 
  print "This should not happen..."
  print("Error: invalid processor ids!")
  exit (1)

#--pdb_home=...
#--bmrb_home=...
pdb_home=""
bmrb_home=""
if command_args.has_key("pdb_home"): 
  pdb_home=command_args["pdb_home"]
if command_args.has_key("bmrb_home"): 
  bmrb_home=command_args["bmrb_home"]

#--workdir=...
if not command_args.has_key("workdir"): 
  print("Error: missing working directory!")
  exit (1)
workdir=command_args["workdir"] 

outdb="%s/nmr_parallel_%d.db" % (workdir, my_id)

debug = False
#--debug=[0|1]
if command_args.has_key("debug"): 
  debug = True

if debug:
  # for debugging
  print " ------------------"
  print " run_parallel.py:"
  print " ------------------"
  print " ** mapping file:", mappingfilename 
  print " ** db_name: ", db_name
  print " ** my_id: ", my_id   
  print " ** num_threads: ",num_threads 
  print " ** pdb_home: ", pdb_home
  print " ** bmrb_home: ", bmrb_home
  print " ** workdir: " , workdir
  print " ** outdb: ", outdb
  print " ------------------"


####
####   split into parallel processes
####

directory = "%s/run_parallel_tmp_new_%d" % (workdir, my_id)
#print " ** directory: ", directory


if (my_id >= num_threads):
  print "This should not happen..."
  exit(1)

try:
  os.rmdir(directory)
except Exception as e:
  print e

try:
  os.mkdir(directory)
except Exception as e:
  print e

try:
  os.chdir(directory)
except Exception as e:
  print e

#print os.getcwd()

# cp the db_file  
try:
  shutil.copy(db_name, outdb)
  print "** Created temporary db %s from %s" % (outdb, db_name)
except Exception as e:
  print e


# read the mapping ... 
map = open(mappingfilename, 'r').readlines() 
thread_length = len(map)/num_threads
#print thread_length

start = my_id*thread_length
end   = (my_id+1)*thread_length

if (my_id == num_threads - 1):
  end = len(map)

#print "**", my_id, len(map), thread_length, start, end
for m in map[start:end]:
  temp = m.strip().split()
  pdb_id  =  temp[0]
  bmrb_id =  temp[1]
  # check if full path or the std directory
  if (os.path.basename(pdb_id)==pdb_id):
    pfile = (pdb_home  + "%s.pdb") % temp[0]
  else:
    pfile = pdb_id
  
  if (os.path.basename(bmrb_id)==bmrb_id):
    bfile = (bmrb_home + "%s.str") % temp[1]
  else:
    bfile = bmrb_id
 
  print "** Call ComputeNMRDescriptors for : ", bfile, pfile, outdb
  print (os.popen("echo $PATH").readlines())
  print (os.popen("pwd").readlines())
  # TODO: handle the ligand :-)
  cmd = "ComputeNMRDescriptors -bmrb_file %s -pdb_file %s -feature_file %s -outfile %s LIGAND -log_file %s/logs/sql/%s.pdb.log -app_type training" % (bfile, pfile, featurefilename, outdb, workdir, pdb_id)
  if (not ligand):
    cmd = "ComputeNMRDescriptors -bmrb_file %s -pdb_file %s -feature_file %s -outfile %s -log_file %s/logs/sql/%s.pdb.log -app_type training" % (bfile, pfile, featurefilename, outdb, workdir, pdb_id)
  os.system(cmd)
  print "** Last command: ", cmd

os.chdir(command_args["workdir"])
os.rmdir(directory)

print "Done."
