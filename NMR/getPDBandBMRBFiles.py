#!/usr/bin/python

import urllib2, xml.dom.minidom
import shutil
from os import path, environ
import os, sys

from nmr_definitions import *

#TODO: in allgemeines log-dir per PID 
logfilename = "%s/mapping.log"  % datadir


############ some definitions #############

def createDir(directory):
	try:
		os.mkdir(directory)
	except Exception as e:
		print e

createDir(pdbhome)
createDir(pdbhome_ligand)
createDir(pdbhome_dna)
createDir(pdbhome_dna_ligand)
createDir(bmrbhome)
createDir(bmrbhome_ligand)
createDir(bmrbhome_dna)
createDir(bmrbhome_dna_ligand)

url = 'http://www.rcsb.org/pdb/rest/'
pdbdownload_url = "http://www.rcsb.org/pdb/files/"
bmrbdownload_url = "http://rest.bmrb.wisc.edu/bmrb/NMR-STAR2/"

queryText_ligand = """
<orgPdbCompositeQuery version="1.0">
 <queryRefinement>
  <queryRefinementLevel>0</queryRefinementLevel>
  <orgPdbQuery>
    <version>head</version>
    <queryType>org.pdb.query.simple.ExpTypeQuery</queryType>
    <description>Experimental Method is SOLUTION NMR and has Experimental Data</description>
    <mvStructure.expMethod.value>SOLUTION NMR</mvStructure.expMethod.value>
    <mvStructure.hasExperimentalData.value>Y</mvStructure.hasExperimentalData.value>
  </orgPdbQuery>
 </queryRefinement>
 <queryRefinement>
  <queryRefinementLevel>1</queryRefinementLevel>
  <conjunctionType>and</conjunctionType>
  <orgPdbQuery>
    <version>head</version>
    <queryType>org.pdb.query.simple.NoLigandQuery</queryType>
    <description>Ligand Search : Has free ligands=yes</description>
    <haveLigands>yes</haveLigands>
  </orgPdbQuery>
 </queryRefinement>
</orgPdbCompositeQuery>
"""

queryText_protein = """
<orgPdbCompositeQuery version="1.0">
 <queryRefinement>
  <queryRefinementLevel>0</queryRefinementLevel>
  <orgPdbQuery>
    <version>head</version>
    <queryType>org.pdb.query.simple.ExpTypeQuery</queryType>
    <description>Experimental Method is SOLUTION NMR</description>
    <mvStructure.expMethod.value>SOLUTION NMR</mvStructure.expMethod.value>
  </orgPdbQuery>
 </queryRefinement>
</orgPdbCompositeQuery>
"""

# cuttoff query does not work currently!!
queryText_cutoff_90 = """
<orgPdbCompositeQuery version="1.0">
 <queryRefinement>
  <queryRefinementLevel>0</queryRefinementLevel>
  <orgPdbQuery>
    <version>head</version>
    <queryType>org.pdb.query.simple.ExpTypeQuery</queryType>
    <description>Experimental Method is SOLUTION NMR</description>
    <mvStructure.expMethod.value>SOLUTION NMR</mvStructure.expMethod.value>
  </orgPdbQuery>
 </queryRefinement>
 <queryRefinement>
  <queryRefinementLevel>1</queryRefinementLevel>
  <conjunctionType>and</conjunctionType>
  <orgPdbQuery>
    <version>head</version>
    <queryType>org.pdb.query.simple.HomologueReductionQuery</queryType>
    <description>Homologue Removal - 90% Identity Cutoff of Experimental Method is SOLUTION NMR
</description>
    <identityCutoff>90</identityCutoff>
  </orgPdbQuery>
 </queryRefinement>
</orgPdbCompositeQuery>
"""

####################################################
##
##              helper functions
##
####################################################

def getDNAList():
  req = urllib2.Request("ftp://ftp.wwpdb.org/pub/pdb/derived_data/pdb_entry_type.txt")
  for m in urllib2.urlopen(req).readlines():
    l = m.split('\t')
    #print l
    if (len(l)>2):
      if (    (l[1].strip() == "nuc" or l[1].strip() == "prot-nuc") 
          and (l[2].strip() == "NMR")):
        nmr_dnas[l[0].strip().upper()] = 1
        #print m
  #print nmr_dnas

def preparePDBBMRBMapping():
  req = urllib2.Request(official_mapping_url)
  for m in urllib2.urlopen(req).readlines():
    l = m.split(",")
    mapping[l[1].strip()] = l[0].strip()

def download(pdbid):
  # get info whether pdb structure contains a ligand 
  xmldata = ""
  try:
    req = urllib2.Request(url+'ligandInfo?structureId='+pdbid)
  except Exception as e:
    print "Could not download pdb " + pdbid
    logfile.write(pdbid+" XXX DEFECT\n")
    return
  try:
    xmldata=urllib2.urlopen(req).read()
    document = xml.dom.minidom.parseString(xmldata)
  except Exception as e: 
    print "Could not parse the following xml: "+xmldata
    logfile.write(pdbid+" XXX DEFECT\n")
    return

  bmrbid = "\033[35;1m NOTFOUND \033[0m"
  if (mapping.has_key(pdbid)):
    bmrbid = mapping[pdbid]
  else:
    logfile.write("No bmrb file for " + pdbid +"\n")
    return

  has_complex_ligand = False
  has_ion = False
  has_dna = nmr_dnas.has_key(pdbid)
  
  # count the ligands
  for ligand in document.getElementsByTagName("ligand"):
    result = ""

    chemName = ""
    chemFormula = ""
    for name in ligand.getElementsByTagName("chemicalName"):
      chemName =  name.childNodes[0].nodeValue.strip()+";"
    for formula in ligand.getElementsByTagName("formula"):
      chemFormula =  formula.childNodes[0].nodeValue.strip()

    result = pdbid + "/" + bmrbid + " " + chemName + " " + chemFormula
    if (" ION;" in chemName):
      print '\033[31;1m' + result + '\033[0m'
      has_ion = True
    else:
      has_complex_ligand = True
      print '\033[32;1m' + result + '\033[0m'

  # download if not already there
  localpath = pdbhome   + "/"
  bmrbpath  = bmrbhome  + "/"
  
  if (has_complex_ligand or has_ion):
    bmrbpath  = bmrbhome_ligand + "/"
    localpath = pdbhome_ligand  + "/"
 
  if (has_dna and (has_complex_ligand or has_ion)):
    bmrbpath  = bmrbhome_dna_ligand + "/"
    localpath = pdbhome_dna_ligand  + "/"
    print '\033[33;1m' + pdbid + "/" + bmrbid + " / DNA-LIGAND " + '\033[0m'
  elif(has_dna):
    bmrbpath  = bmrbhome_dna + "/"
    localpath = pdbhome_dna  + "/"
    print '\033[34;1m' + pdbid + "/" + bmrbid + " / DNA " + '\033[0m'
  elif(not has_complex_ligand and not has_ion and not has_dna):
    localpath = pdbhome   + "/"
    bmrbpath  = bmrbhome  + "/"
    print '\033[35;1m' + pdbid + " / " + bmrbid + " [SINGLE] " + '\033[0m'

  if (not ("NOTFOUND" in bmrbid) ):
    #and not has_complex_ligand and not has_ion):
    if (not path.exists(localpath+pdbid+".pdb") 
        or not path.exists(bmrbpath+bmrbid+".str") ):
       web = urllib2.urlopen(pdbdownload_url+pdbid+".pdb")
       localfile = open(localpath+pdbid+".pdb", 'w')
       localfile.write(web.read())
       localfile.close()

       try:
         web = urllib2.urlopen(bmrbdownload_url+bmrbid)
       except Exception as e: 
         print "Could not download bmrb file "+bmrbid
         logfile.write(bmrbid+" XXX DEFECT\n")
         return

       localfile = open(bmrbpath+"/"+bmrbid+".str", 'w')
       localfile.write(web.read())
       localfile.close()

    type = "SINGLE"
    if ((has_complex_ligand or has_ion) and not has_dna):
      type = "LIGAND"
      mappingfile_ligand.write(pdbid+" "+bmrbid+" "+type+"\n")
    elif (not has_complex_ligand and not has_ion and has_dna):
      type = "DNA"
      mappingfile_dna.write(pdbid+" "+bmrbid+" "+type+"\n")
    if ((has_complex_ligand or has_ion) and has_dna):
      type = "DNA_LIGAND"
      mappingfile_dna_ligand.write(pdbid+" "+bmrbid+" "+type+"\n")
    # type single
    if (not has_complex_ligand and not has_ion and not has_dna):
      mappingfile.write(pdbid+" "+bmrbid+" "+type+"\n")

####################################################
##
##
##              MAIN
##
##
####################################################
if (len(sys.argv) < 2):
  print "Usage: %s --mappingfilename=[...]" % sys.argv[0] 
  exit (1)

mapping  = {}
nmr_dnas = {}

# parse the command line arguments
command_args = {}
for arg in sys.argv[1:]:
  sp = arg.split("=")
  command_args[sp[0][2:]] = sp[1]

### mappingfilename
if not command_args.has_key("mappingfilename"):
  print("Error: invalid mapping file name!")
  exit (1)
mappingfilename = command_args["mappingfilename"]

mappingfilename_ligand = mappingfilename + "_LIGAND.txt"
if command_args.has_key("mappingfilename_ligand"):
  mappingfilename_ligand = command_args["mappingfilename_ligand"]

mappingfilename_dna = mappingfilename + "_DNA.txt"
if command_args.has_key("mappingfilename_dna"):
  mappingfilename_dna = command_args["mappingfilename_dna"]

mappingfilename_dna_ligand = mappingfilename + "_DNA_LIGAND.txt"
if command_args.has_key("mappingfilename_dna_ligand"):
  mappingfilename_dna_ligand = command_args["mappingfilename_dna_ligand"]

mappingfile            = open(mappingfilename, 'w')
mappingfile_ligand     = open(mappingfilename_ligand, 'w')
mappingfile_dna        = open(mappingfilename_dna, 'w')
mappingfile_dna_ligand = open(mappingfilename_dna_ligand, 'w')

official_mapping_url="http://www.bmrb.wisc.edu/ftp/pub/bmrb/nmr_pdb_integrated_data/adit_nmr_matched_pdb_bmrb_entry_ids.csv"
if command_args.has_key("official_mapping_url"):
  official_mapping_url = command_args["official_mapping_url"]

logfile            = open(logfilename, 'w')

##
##  mapping
##

print "Get PDB DNA list...\n"
getDNAList()

print "Prepare mapping...\n"
preparePDBBMRBMapping()

print "Write DNA PDB-BMRB mapping...\n"
counter = 0
for d in nmr_dnas: 
  #print d
  if (mapping.has_key(d)):
    bmrbid = mapping[d]
    mappingfile_dna.write(d+ "  " + bmrbid + "\n")
    counter= counter+1
print "Found number of DNA entries: ", counter, "\n"

print "Perform query:\n", queryText_protein #queryText_cutoff_90 #queryText

print "Querying PDB...\n"

req = urllib2.Request(url+'search', data=queryText_protein)#queryText_cutoff_90) #queryText

f = urllib2.urlopen(req)

result = f.read()

if result:

    print "Found number of PDB entries:", result.count('\n')

    for id in result.split("\n"):
	    print id
	    download(id.strip())
    
    print "log file stored in:", logfilename      
    print "Protein        PDB-BMRB mapping stored in:", mappingfilename  
    print "Protein-Ligand PDB-BMRB mapping stored in:", mappingfilename_ligand  
    print "DNA            PDB_BMRB mapping stored in:", mappingfilename_dna      
    print "DNA-Ligand     PDB_BMRB mapping stored in:", mappingfilename_dna_ligand      
    
    print "Protein        PDB's stored in:",  pdbhome   
    print "Protein-Ligand PDB's stored in:",  pdbhome_ligand 
    print "DNA            PDB's stored in:",  pdbhome_dna 
    print "DNA-Ligand     PDB's stored in:",  pdbhome_dna_ligand 
      
    print "Protein        BMRB files stored in: ", bmrbhome  
    print "Protein-Ligand BMRB files stored in: ", bmrbhome_ligand  
    print "DNA            BMRB files stored in: ", bmrbhome_dna  
    print "DNA-Ligand     BMRB files stored in: ", bmrbhome_dna_ligand  

else:

    print "Failed to retrieve results" 

mappingfile.close()
mappingfile_ligand.close()
mappingfile_dna.close()
mappingfile_dna_ligand.close()
logfile.close()

print "Done"
