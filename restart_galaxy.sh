#!/bin/bash

GALAXY_ROOT_DIR=/home/AH/nicste/PROMO/GALAXY/new

export LD_LIBRARY_PATH=$GALAXY_ROOT_DIR/BALL/build/lib:
export PYTHONPATH=$GALAXY_ROOT_DIR/BALL/build/lib
export BALL_DATA_PATH=$GALAXY_ROOT_DIR/BALL/data
export PATH=$GALAXY_ROOT_DIR/BALL/build/bin:$PATH

ps -ef | grep python | grep paster | awk '{print $2}' 
ps -ef | grep python | grep paster | awk '{print $2}' | xargs kill

cd $GALAXY_ROOT_DIR/ballaxy
nohup ./run.sh > galaxy.log &
