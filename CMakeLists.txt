#The name of the project to build
PROJECT(BALL_GALAXY)

# We currently require at least version 2.6 of cmake
CMAKE_MINIMUM_REQUIRED(VERSION 2.6)

# The path to the BALLConfig.cmake file. Usually located
# in the build directory of your BALL installation
IF (CMAKE_BUILD_TYPE STREQUAL "Debug")
	SET(BALL_DIR "/home/HPL/anne/BALL/BALL-NMR/build_debug/cmake")
ELSE()
	SET(BALL_DIR "/home/HPL/anne/BALL/BALL-NMR/build/cmake")
ENDIF()

# Make cmake read the BALL configuration files
FIND_PACKAGE(BALL REQUIRED)

# Set BALL specific includes and compilerflags
INCLUDE_DIRECTORIES(${BALL_INCLUDE_DIRS} ${PROJECT_SOURCE_DIR} "NMR")
ADD_DEFINITIONS(${BALL_DEFINITIONS})
ADD_DEFINITIONS(${BALL_ADD_CXXFLAGS})

# Tell cmake to generate an executable called example that depends
# on the file example.cpp
ADD_EXECUTABLE(PDBCutter PDBCutter.C)
ADD_EXECUTABLE(BondOrderAssigner BondOrderAssigner.C)
ADD_EXECUTABLE(MolecularFileConverter MolecularFileConverter.C)
ADD_EXECUTABLE(CrystalGenerator CrystalGenerator.C)
ADD_EXECUTABLE(Split2ConnectedComponents Split2ConnectedComponents.C)
ADD_EXECUTABLE(ComputeNMRDescriptors NMR/SQLiteConnector.C NMR/ComputeNMRDescriptors.C NMR/propertiesForShift.C)
ADD_EXECUTABLE(readNMR_PH NMR/readNMR_PH.C)
ADD_EXECUTABLE(SQLiteProperties NMR/SQLiteProperties.C NMR/SQLiteConnector.C NMR/propertiesForShift.C)

# Link example against BALL
#TARGET_LINK_LIBRARIES(example BALL)
TARGET_LINK_LIBRARIES(PDBCutter BALL)
TARGET_LINK_LIBRARIES(BondOrderAssigner BALL) 
TARGET_LINK_LIBRARIES(MolecularFileConverter BALL) 
TARGET_LINK_LIBRARIES(CrystalGenerator BALL) 
TARGET_LINK_LIBRARIES(Split2ConnectedComponents BALL)
TARGET_LINK_LIBRARIES(ComputeNMRDescriptors BALL sqlite3) 
TARGET_LINK_LIBRARIES(readNMR_PH BALL)
TARGET_LINK_LIBRARIES(SQLiteProperties BALL sqlite3)
